import Service from '@ember/service';
import { inject } from '@ember/service';
import Evented from '@ember/object/evented';
import { Promise, resolve } from 'rsvp';

export default Service.extend(Evented, {

  ajax: inject('ajax'),

  notifications: inject('notification-messages'),
  platform: inject('ember-cordova/platform'),

  name: 'io.byebug.pushnotification',
  registerUrl: '/devices/register',
  updatePushUrl: '/devices/update-push',

  requested: null,

  isEnabled: null,

  registration: null,

  notification: null,

  error: null,

  init() {
    this._super(...arguments);
    // this.get('notifications').setDefaultAutoClear(true);
    // this.get('notifications').setDefaultClearDuration(1200);

    this.set('requested', JSON.parse(localStorage.getItem('ember-cordova-push-requested')));
    this.checkPermission();
  },

  register() {
    // this.get('notifications').info('register with server');
    return this.get('ajax').request(this.get('registerUrl'), {
      method: 'POST',
      data: {
        name: this.get('name'),
        platform: this.get('platform.device.platform'),
        version: this.get('platform.device.version'),
        uuid: this.get('platform.device.uuid'),
        cordova: this.get('platform.device.cordova'),
        model: this.get('platform.device.model'),
        manufacturer: this.get('platform.device.manufacturer'),
        serial: this.get('platform.device.serial'),
      }
    });
  },

  updatePush() {
    return this.get('ajax').request(this.get('updatePushUrl'), {
      method: 'POST',
      data: {
        name: this.get('name'),
        registration_type: this.get('registration.registrationType'),
        registration_id: this.get('registration.registrationId'),
        uuid: this.get('platform.device.uuid'),
      }
    });
  },

  unregister() {
    this.get('push').unregister(() => {
      // this.get('notifications').success('unregistered');
    }, error => {
      // this.get('notifications').error(error);
    });
  },

  openSettings() {
    // this.get('notifications').info('open notification settings');
    cordova.plugins.settings.open("notification_id", function() {
        // this.get('notifications').info('notification settings opened');
      },
      function() {
        // this.get('notifications').info('error opennotification settings');
      }
    );
  },

  checkPermission() {
    // this.get('notifications').info('check permission');
    PushNotification.hasPermission(data => {
      if (data.isEnabled) {
        // this.get('notifications').success('notification enabled');
        this.set('isEnabled', data.isEnabled);
      } else {
        // this.get('notifications').success('notification NOT enabled');
        this.set('isEnabled', data.isEnabled);
      }
    });
  },

  initPush() {

    this.set('requested',true);
    localStorage.setItem('ember-cordova-push-requested',JSON.stringify(true));

    // this.get('notifications').info('init push');
    var push = PushNotification.init({
      android: {},
      ios: {
        alert: "true",
        badge: "true",
        sound: "true",
        clearBadge: "true"
      }
    });
    this.set('push', push);

    let promise = new Promise((resolve, reject) => {
      push.on('registration', (data) => {
        this.set('registration', data);
        resolve(data);
      }), push.on('error', (e) => {
        this.set('error', e);
        reject(e);
      });
    });

    // push.on('registration', (data) => {
    //   // data.registrationId
    //   // this.get('notifications').success('on registration');
    //   this.set('registration', data);
    // });

    push.on('notification', (data) => {
      // this.get('notifications').info(data.message);
      // data.message,
      // data.title,
      // data.count,
      // data.sound,
      // data.image,
      // data.additionalData
      // this.set('notification', data);
      this.trigger('notification',data);
    });

    // push.getApplicationIconBadgeNumber(
    //   n => {
    //     console.log('success', n);
    //   },
    //   () => {
    //     console.log('error');
    //   }
    // );
    // push.setApplicationIconBadgeNumber(
    //   () => {
    //     console.log('success');
    //   },
    //   () => {
    //     console.log('error');
    //   },
    //   7
    // );

    // push.on('error', (e) => {
    //   // e.message
    //   // this.get('notifications').error('on error');
    //   this.set('error', e);
    // });

  }

});
